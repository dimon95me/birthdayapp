package examplesoureit.yourbirth;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatisticActivity extends AppCompatActivity {

    @BindView(R.id.years_count)
    TextView yearsCount;
    @BindView(R.id.days_count)
    TextView daysCount;
    @BindView(R.id.seconds_count)
    TextView secondsUp;
    @BindView(R.id.zodiac)
    TextView zodiacTextView;

    GregorianCalendar birthDate;
    GregorianCalendar nowDate;
    String birthDay;
    String birthMonth;
    String birthYear;

    int seconds = 0;
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            secondsUp.setText("You run seconds: " + seconds);
            handler.postDelayed(runnable, 1000);
            seconds++;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        birthDay = intent.getStringExtra("day");
        birthMonth = intent.getStringExtra("month");
        birthYear = intent.getStringExtra("year");

        birthDate = new GregorianCalendar();
        birthDate.set(Integer.parseInt(birthDay), Integer.parseInt(birthMonth), Integer.parseInt(birthYear));
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

//        Date birthDate;
        nowDate = new GregorianCalendar();


        yearsCount.setText("You are have " + getYearsOld() + " years");
        daysCount.setText("You are have " + getDaysOld() + " days");


        runnable.run();//Через пример, поданный на уроке не получилось. Пришлось воспользоваться таким методом

        zodiacTextView.setText("Your zodiac is: "+ getZodiac(birthDay, birthMonth));


//        Toast.makeText(this, String.valueOf(getDaysOld()), Toast.LENGTH_SHORT).show();
    }

    private int getYearsOld() {
        int yearsOld = nowDate.get(GregorianCalendar.YEAR) - Integer.parseInt(birthYear);
        if (nowDate.get(GregorianCalendar.MONTH) >= Integer.parseInt(birthMonth) - 1 && nowDate.get(GregorianCalendar.DAY_OF_MONTH) < Integer.parseInt(birthDay)) {
            yearsOld--;
        }
        return yearsOld;
    }


    private int getDaysOld() {
        Calendar birthCalendar = Calendar.getInstance();
        birthCalendar.set(Calendar.DAY_OF_MONTH, (Integer.parseInt(birthDay)));
        birthCalendar.set(Calendar.MONTH, Integer.parseInt(birthMonth) - 1);
        birthCalendar.set(Calendar.YEAR, (Integer.parseInt(birthYear)));

        Date yourBirthDate = birthDate.getTime();
        Date yourCurrentDate = new Date();

        return (int) ((yourCurrentDate.getTime() - yourBirthDate.getTime()) / 1000 / 60 / 60 / 60 / 24);
    }


    private String getZodiac(String day, String month) {

        String result = "Null";

        switch (month) {
            case "01":
                if (Integer.parseInt(day) <= 20) {
                    result = "Козерог";
                } else {
                    result = "Водолей";
                }
                break;
            case "02":
                if (Integer.parseInt(day) <= 18) {
                    result = "Водолей";
                } else {
                    result = "Рыбы";
                }
                break;
            case "03":
                if (Integer.parseInt(day) <= 20) {
                    result = "Рыбы";
                } else {
                    result = "Овен";
                }
                break;
            case "04":
                if (Integer.parseInt(day) <= 20) {
                    result = "Овен";
                } else {
                    result = "Телец";
                }
                break;
            case "05":
                if (Integer.parseInt(day) <= 20) {
                    result = "Телец";
                } else {
                    result = "Близнецы";
                }
                break;
            case "06":
                if (Integer.parseInt(day) <= 21) {
                    result = "Близнецы";
                } else {
                    result = "Рак";
                }
                break;
            case "07":
                if (Integer.parseInt(day) <= 22) {
                    result = "Рак";
                } else {
                    result = "Лев";
                }
                break;
            case "08":
                if (Integer.parseInt(day) <= 22) {
                    result = "Лев";
                } else {
                    result = "Дева";
                }
                break;
            case "09":
                if (Integer.parseInt(day) <= 22) {
                    result = "Дева";
                } else {
                    result = "Весы";
                }
                break;
            case "10":
                if (Integer.parseInt(day) <= 23) {
                    result = "Весы";
                } else {
                    result = "Скорпион";
                }
                break;
            case "11":
                if (Integer.parseInt(day) <= 22) {
                    result = "Скорпион";
                } else {
                    result = "Стрелец";
                }
                break;
            case "12":
                if (Integer.parseInt(day) <= 21) {
                    result = "Стрелец";
                } else {
                    result = "Водолей";
                }
                break;
            default:
                return "Null";
        }

        return result;

    }
}
