package examplesoureit.yourbirth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

//    Intent intent;

//    @BindView(R.id.action)
//    Button action;
    @BindView(R.id.day)
    EditText day;
    @BindView(R.id.month)
    EditText month;
    @BindView(R.id.year)
    EditText year;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

    }

@OnClick(R.id.action) void onClick(){

        if (!day.getText().toString().startsWith("0") &&  day.getText().toString().length()==1){
            String wrongDay = day.getText().toString();
    day.setText("0"+wrongDay);
    }
    if (!month.getText().toString().startsWith("0") &&  month.getText().toString().length()==1){
        String wrongMonth = month.getText().toString();
        month.setText("0"+wrongMonth);
    }
    if (year.getText().toString().length()==2){
            String wrongYear = year.getText().toString();
            year.setText("19"+wrongYear);
    }
        String date = day.getText()+"."+month.getText()+"."+year.getText();

//    Toast.makeText(this, date, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(MainActivity.this, StatisticActivity.class);
        intent.putExtra("day",day.getText().toString());
        intent.putExtra("month", month.getText().toString());
        intent.putExtra("year", year.getText().toString());
        startActivity(intent);

    }

}
